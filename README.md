Auto IT User
v.2

1. Create user with appropriate role and permissions
     (the module ignores anonymous and admin)
2. Enable module
3. go to /admin/user_mapping
4. Choose:
     User
     Authentication key
     Page to redirect user to

When you go to the site with the ?key=<key> you will be automatically
logged in as that user.

There can only be one user assigned, with one key.
The module sets the module data as a json object in the systems' variables.

Added the page to redirect to.


http://drupal-7.dd:8083/?key=B00782E74DC753A520FFD0C24ADD144DC2CE7575CF2DBC2F9DA04CB0A0A02642
